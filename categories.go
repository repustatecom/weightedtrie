package weightedtrie

import "sync"

type categoryId int32

type categoriesIdx struct {
	sync.Mutex
	index       map[categoryId]string
	invertedIdx map[string]categoryId
	cnt         categoryId
}

func newCategoriesIdx() categoriesIdx {
	return categoriesIdx{
		index:       make(map[categoryId]string),
		invertedIdx: make(map[string]categoryId),
		cnt:         0,
	}
}

func (c *categoriesIdx) add(category string) categoryId {
	c.Lock()
	defer c.Unlock()
	if id, found := c.exists(category); found {
		return id
	}

	c.cnt++
	c.index[c.cnt] = category
	c.invertedIdx[category] = c.cnt
	return c.cnt
}

func (c categoriesIdx) exists(category string) (categoryId, bool) {
	id, ok := c.invertedIdx[category]
	return id, ok
}

func (c categoriesIdx) getById(id categoryId) (string, bool) {
	c.Lock()
	category, ok := c.index[id]
	c.Unlock()
	return category, ok
}
