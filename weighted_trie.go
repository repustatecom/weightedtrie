package weightedtrie

import (
	"bufio"
	"bytes"
	"encoding/gob"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"sync"
)

type Trie struct {
	sync.Mutex
	root       *branch
	categories categoriesIdx
}

/*
NewTrie returns the pointer to a new Trie with an initialized root Branch
*/
func New() *Trie {
	t := &Trie{
		root: &branch{
			Branches: make(map[rune]*branch),
		},
		categories: newCategoriesIdx(),
	}
	return t
}

/*
Add adds an entry to the trie
*/
func (t *Trie) Add(val, cat string, w float64) {
	// validate category in the index
	id := t.categories.add(cat)

	// add to the trie
	t.Lock()
	t.root.add([]rune(val), val, id, w)
	t.Unlock()
}

/*
Find returns all entries of the Trie that have the given prefix
with their counts as Leaf sorted by weights
*/
func (t *Trie) Find(prefix string) Leafs {
	leafs := t.root.prefixMembers([]rune{}, []rune(prefix))
	plain := createLeafs(leafs, t.categories)
	sort.Sort(plain)

	return plain
}

/*
FindFirstN returns first N results for given prefix sorted by weights
*/
func (t *Trie) FindFirstN(prefix string, n int) Leafs {
	leafs := t.Find(prefix)

	if len(leafs) <= n {
		return leafs
	}

	return leafs[:n]
}

/*
SaveToFile dumps all values into a slice of strings and writes that to a file
using encoding/gob.

The Trie itself can currently not be encoded directly because gob does not
directly support structs with a sync.Mutex on them.
*/
func (t *Trie) SaveToFile(fname string) (err error) {
	t.Lock()
	entries := t.Find("")
	defer t.Unlock()

	buf := new(bytes.Buffer)
	enc := gob.NewEncoder(buf)
	if err = enc.Encode(entries); err != nil {
		err = errors.New(fmt.Sprintf("Could encode Trie entries for dump file: %v", err))
		return
	}

	f, err := os.Create(fname)
	if err != nil {
		err = errors.New(fmt.Sprintf("Could not save dump file: %v", err))
		return
	}
	defer f.Close()

	w := bufio.NewWriter(f)
	_, err = w.Write(buf.Bytes())
	if err != nil {
		err = errors.New(fmt.Sprintf("Error writing to dump file: %v", err))
		return
	}

	w.Flush()
	return
}

/*
LoadFromFile loads a gib encoded wordlist from a file and creates a new Trie
by Add()ing all of them.
*/
func LoadFromFile(fname string) (tr *Trie, err error) {
	tr = New()
	entries, err := loadTrieFile(fname)
	if err != nil {
		return
	}
	for _, mi := range entries {
		tr.Add(mi.Value, mi.Category, mi.Weight)
	}

	return
}

func loadTrieFile(fname string) (entries Leafs, err error) {
	f, err := os.Open(fname)
	if err != nil {
		err = errors.New(fmt.Sprintf("Could not open Trie file: %v", err))
	} else {
		defer f.Close()

		buf := bufio.NewReader(f)
		dec := gob.NewDecoder(buf)
		if err = dec.Decode(&entries); err != nil {
			if err == io.EOF && entries == nil {
				log.Println("Nothing to decode. Seems the file is empty.")
				err = nil
			} else {
				err = errors.New(fmt.Sprintf("Decoding error: %v", err))
				return
			}
		}
	}

	return
}
