package weightedtrie

import (
	"fmt"
	"strings"
)

type branch struct {
	Branches  map[rune]*branch
	LeafValue []rune
	Leaf      *leaf
	End       bool
}

/*
NewBranch returns a new initialezed *Branch
*/
func (b *branch) newBranch() *branch {
	return &branch{
		Branches: make(map[rune]*branch),
	}
}

/*
Add adds an entry to the Branch
*/
func (b *branch) add(entry []rune, value string, id categoryId, weight float64) (addedBranch *branch) {
	if b.LeafValue == nil && len(b.Branches) == 0 {
		if len(entry) > 0 {
			b.LeafValue = entry
		} else {
			// something came in but we already have branches for it
			// so the tail was the current branches index but no value
			// to push. just mark the current idx position as End
		}
		b.End = true
		// if leaf is missing, create one
		if b.Leaf == nil {
			b.Leaf = &leaf{value, []categoryId{id}, weight}
			// otherwise add only category
		} else {
			b.Leaf.categories = append(b.Leaf.categories, id)
		}
		addedBranch = b
		return
	}

	// check the overlap between the current LeafValue and the new entry
	newLeaf := func(LeafValue, newEntry []rune) (leaf []rune) {
		for li, b := range LeafValue {
			if li > len(newEntry)-1 {
				break
			}
			if b == newEntry[li] {
				leaf = append(leaf, b)
			} else {
				break
			}
		}
		return
	}(b.LeafValue, entry)

	newLeafLen := len(newLeaf)

	// the new leaf is smaller than the current leaf.
	// we will push the old leaf down the branch
	if newLeafLen < len(b.LeafValue) {
		tail := b.LeafValue[newLeafLen:]
		idx := tail[0]
		newBranch := b.newBranch()
		newBranch.LeafValue = tail[1:]

		b.LeafValue = newLeaf
		newBranch.Branches, b.Branches = b.Branches, newBranch.Branches
		newBranch.End, b.End = b.End, newBranch.End

		if newBranch.End {
			newBranch.Leaf = b.Leaf
		}

		b.Branches[idx] = newBranch
	}

	// new leaf is smaller than the entry, which means there will be more stuff
	// that we need to push down
	if newLeafLen < len(entry) {
		tail := entry[newLeafLen:]
		idx := tail[0]

		// create new branch at idx if it does not exists yet
		if _, notPresent := b.Branches[idx]; !notPresent {
			b.Branches[idx] = b.newBranch()
		}
		// check whether the idx itself marks an End $. if so add a new idx
		addedBranch = b.Branches[idx].add(tail[1:], value, id, weight)
	} else {
		// if there is nothing else to be pushed down we just have to mark the
		// current branch as an end. this happens when you add a value that already
		// is covered by the index but this particular end had not been marked.
		// eg. you already have 'food' and 'foot' (shared LeafValue of 'foo') in
		// your index and now add 'foo'.
		// if leaf is missing, create one
		if b.Leaf == nil {
			b.Leaf = &leaf{value, []categoryId{id}, weight}
			// otherwise add only category
		} else {
			b.Leaf.categories = append(b.Leaf.categories, id)
		}
		b.End = true
		addedBranch = b
	}
	return addedBranch
}

/*
Members returns slice of all Members of the Branch prepended with `branchPrefix`
*/
func (b *branch) members(branchPrefix []rune) (members []*leaf) {
	if b.End {
		members = append(members, b.Leaf)
	}
	for idx, br := range b.Branches {
		newPrefix := append(append(branchPrefix, b.LeafValue...), idx)
		members = append(members, br.members(newPrefix)...)
	}
	return
}

/*
prefixMembers returns a slice of all Members of the Branch matching the given prefix. The values returned are prepended with `branchPrefix`
*/
func (b *branch) prefixMembers(branchPrefix []rune, searchPrefix []rune) (members []*leaf) {
	exists, br, matchedPrefix := b.hasPrefixBranch(searchPrefix)
	if exists {
		members = br.members(matchedPrefix)
	}
	return
}

/*
 */
func (b *branch) has(entry []rune) bool {
	return b.getBranch(entry) != nil
}

/*
 */
func (b *branch) getBranch(entry []rune) (be *branch) {
	leafLen := len(b.LeafValue)
	entryLen := len(entry)

	if entryLen >= leafLen {
		for i, pb := range b.LeafValue {
			if pb != entry[i] {
				return
			}
		}
	} else {
		return
	}

	if entryLen > leafLen {
		if br, present := b.Branches[entry[leafLen]]; present {
			return br.getBranch(entry[leafLen+1:])
		} else {
			return
		}
	} else if entryLen == leafLen && b.End {
		be = b
	}
	return
}

/*
 */
func (b *branch) hasPrefix(prefix []rune) bool {
	exists, _, _ := b.hasPrefixBranch(prefix)
	return exists
}

/*
 */
func (b *branch) hasPrefixBranch(prefix []rune) (exists bool, bra *branch, matchedPrefix []rune) {
	leafLen := len(b.LeafValue)
	prefixLen := len(prefix)
	exists = false
	var pref []rune

	if leafLen > 0 {
		if prefixLen <= leafLen {
			for i, pb := range prefix {
				if pb != b.LeafValue[i] {
					return
				}
			}
		} else {
			for i, lb := range b.LeafValue {
				if prefix[i] != lb {
					return
				}
			}
			matchedPrefix = append(matchedPrefix, prefix[:leafLen]...)
		}
	}

	if prefixLen > leafLen {
		if br, present := b.Branches[prefix[leafLen]]; present {
			matchedPrefix = append(matchedPrefix, prefix[leafLen])
			exists, bra, pref = br.hasPrefixBranch(prefix[leafLen+1:])
			matchedPrefix = append(matchedPrefix, pref...)
			return
		} else {
			return
		}
	}
	return true, b, matchedPrefix
}

/*
 */
func (b *branch) dump(depth int) (out string) {
	const PADDING_CHAR = "-"
	if len(b.LeafValue) > 0 {
		if b.End {
			out += fmt.Sprintf("%s V:%v %v\n", strings.Repeat(PADDING_CHAR, depth), string(b.LeafValue), b.LeafValue)
		} else {
			out += fmt.Sprintf("%s V:%v %v (%v)\n", strings.Repeat(PADDING_CHAR, depth), string(b.LeafValue), b.LeafValue, "-")
		}
	}

	if b.End {
		out += fmt.Sprintf("%s $\n", strings.Repeat(PADDING_CHAR, depth+len(b.LeafValue)))
	}

	for idx, br := range b.Branches {
		if br.End && len(br.LeafValue) == 0 {
			out += fmt.Sprintf("%s I:%v %v\n", strings.Repeat(PADDING_CHAR, depth+len(b.LeafValue)), string(idx), idx)
		} else {
			out += fmt.Sprintf("%s I:%v %v (%v)\n", strings.Repeat(PADDING_CHAR, depth+len(b.LeafValue)), string(idx), idx, "-")
		}
		out += br.dump(depth + len(b.LeafValue) + 1)
	}

	return
}

/*
 */
func (b *branch) pullUp() *branch {
	if len(b.Branches) == 1 {
		for k, nextBranch := range b.Branches {
			if len(nextBranch.Branches) == 0 {
				b.LeafValue = append(b.LeafValue, append([]rune{k}, nextBranch.LeafValue...)...)
			} else {
				b.LeafValue = append(b.LeafValue, k)
			}
			b.End = nextBranch.End
			b.Branches = nextBranch.Branches
		}
		return b.pullUp()
	}
	return b
}

/*
 */
func (b *branch) String() string {
	return b.dump(0)
}
