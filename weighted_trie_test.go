package weightedtrie

import (
	"bufio"
	"encoding/csv"
	"io"
	"os"
	"strconv"
	"testing"
)

func TestAdd(t *testing.T) {
	tr := New()
	res := tr.Find("")
	if len(res) != 0 {
		t.Fatalf("Find on empty trie: expected 0 results, got %d", len(res))
	}

	tr.Add("Duck", "Animal", 10.0)
	n := 1
	res = tr.Find("")
	if len(res) != n {
		t.Fatalf("Expected %d leafs, got %d", n, len(res))
	}
	tr.Add("Bird", "Animal", 10.0)
	tr.Add("Squirrel", "Animal", 10.0)
	tr.Add("Fox", "Animal", 15.0)
	tr.Add("Wolf", "Animal", 8.0)
	res = tr.Find("")
	n = 5
	if len(res) != n {
		t.Fatalf("Expected %d leafs, got %d", n, len(res))
	}
}

func TestFind(t *testing.T) {
	tr := New()
	res := tr.Find("Animal")
	if len(res) != 0 {
		t.Fatalf("Expected 0 result from empty trie, got %d", len(res))
	}

	tr.Add("Duck", "Animal", 10.0)
	tr.Add("Bird", "Animal", 10.0)
	tr.Add("Bull", "Animal", 10.0)
	tr.Add("Squirrel", "Animal", 10.0)
	tr.Add("Fox", "Animal", 15.0)
	tr.Add("Wolf", "Animal", 8.0)
	res = tr.Find("B")
	n := 2
	if len(res) != n {
		t.Fatalf("Expected %d results, got %d", n, len(res))
	}
}

func TestFindFirstN(t *testing.T) {
	tr := New()
	tr.Add("Donaldsdas Duck", "Cartoon Character", 10.0)
	tr.Add("Donalda Trump", "Cartoon Character", 10.0)
	tr.Add("Donalda Duck", "Cartoon Character", 10.0)
	tr.Add("Donald Trump", "President", 15.0)
	tr.Add("Donald Sutherland", "Actor", 8.0)

	n := 2
	res := tr.FindFirstN("Donald", n)
	if len(res) != n {
		t.Fatalf("Expected %d results, got %d", n, len(res))
	}

	n = 4
	res = tr.FindFirstN("Donald", n)
	if len(res) != n {
		t.Fatalf("Expected %d results, got %d", n, len(res))
	}
}

func TestFindSameValue(t *testing.T) {
	tr := New()

	cat1, cat2 := "Cartoon Character", "President"
	tr.Add("Donald Duck", "Cartoon Character", 10.0)
	tr.Add("Donald Trump", cat1, 15.0)
	tr.Add("Donald Trump", cat2, 15.0)
	tr.Add("Some cool guy Sutherland", "Actor", 8.0)

	res := tr.Find("Donald Trump")
	expN := 2
	if len(res) != expN {
		t.Fatalf("Expected %d results, got %d", expN, len(res))
	}

	if res[0].Category != cat1 && res[0].Category != cat2 {
		t.Fatalf("Result[0]: expected category %s or %s, got %s", cat1, cat2, res[0].Category)
	}

	if res[1].Category != cat1 && res[1].Category != cat2 {
		t.Fatalf("Result[1]: expected category %s or %s, got %s", cat1, cat2, res[1].Category)
	}
}

func TestSaveLoad(t *testing.T) {
	tr := New()
	tr.Add("Donaldsdas Duck", "Cat1", 1125.0)
	tr.Add("Donaldsdas Trump", "Cat2", 1123.0)
	tr.Add("Donald Duck", "Cartoon Character", 10.0)
	tr.Add("Donald Trump", "President", 15.0)
	tr.Add("Donald Sutherland", "Actor", 8.0)
	tr.Add("Abraham Sutherland", "Actor", 100500.0)

	filename := "./trie"

	err := tr.SaveToFile(filename)
	if err != nil {
		t.Fatalf("failed save trie to file: %v", err)
	}

	tr, err = LoadFromFile(filename)
	if err != nil {
		t.Fatalf("failed load trie from file: %v", err)
	}

	n := 2
	res := tr.FindFirstN("Donald", n)
	if len(res) != n {
		t.Fatalf("Expected %d results, got %d", n, len(res))
	}
}

func TestLoadFindHockey(t *testing.T) {
	tr, err := LoadFromFile("testfiles/searchbar.trie")
	if err != nil {
		t.Fatal(err)
	}

	n := 10
	res := tr.FindFirstN("hockey", n)
	if len(res) != n {
		t.Fatalf("Expected %d results, got %d", n, len(res))
	}

	top := res[0]
	expCat := "Event.sport:\"Hockey\""
	if top.Category != expCat {
		t.Fatalf("Top result category should be %s, got %s", expCat, top.Category)
	}
}

func BenchmarkFind(b *testing.B) {
	csvFile, _ := os.Open("testfiles/searchbar.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	reader.Comma = '\t'

	tr := New()
	var w int

	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			b.Fatal(err)
		}
		w, err = strconv.Atoi(line[2])
		tr.Add(line[0], line[1], float64(w))
	}

	tr.SaveToFile("searchbar.trie")

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		tr.FindFirstN("donald", 10)
	}
}
