package weightedtrie

import "fmt"

type leaf struct {
	value      string
	categories []categoryId
	weight     float64
}

func (l *leaf) toPlain(cats categoriesIdx) Leafs {
	res := make(Leafs, 0, len(l.categories))

	for _, id := range l.categories {
		cat, found := cats.getById(id)
		if found {
			res = append(res, &PlainLeaf{
				Value:    l.value,
				Category: cat,
				Weight:   l.weight,
			})
		}
	}

	return res
}

type PlainLeaf struct {
	Value    string
	Category string
	Weight   float64
}

func (m *PlainLeaf) String() string {
	return fmt.Sprintf("%s (%s, %v)", m.Value, m.Category, m.Weight)
}

/*
sorter interface
*/
type Leafs []*PlainLeaf

func createLeafs(leafs []*leaf, cats categoriesIdx) Leafs {
	res := make(Leafs, 0, len(leafs))

	for _, leaf := range leafs {
		res = append(res, leaf.toPlain(cats)...)
	}

	return res
}
func (slice Leafs) Len() int {
	return len(slice)
}

func (slice Leafs) Less(i, j int) bool {
	if slice[i].Weight == slice[j].Weight {
		if slice[i].Value == slice[j].Value {
			return slice[i].Category < slice[j].Category
		}
		return slice[i].Value < slice[j].Value
	}

	return slice[i].Weight > slice[j].Weight
}

func (slice Leafs) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}
